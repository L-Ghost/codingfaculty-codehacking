<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        factory(App\User::class, 10)->create()->each(function($user) {
            $user->posts()->save(factory(App\Post::class)->make());
        });

        factory(App\Role::class, 3)->create();
        factory(App\Category::class, 5)->create();
        factory(App\Photo::class, 1)->create();

        factory(App\Comment::class, 10)->create()->each(function($comment) {
            $comment->replies()->save(factory(App\Reply::class)->make());
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        //$this->call(UsersTableSeeder::class);
    }
}
